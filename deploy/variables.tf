variable "prefix" {
  type        = string
  default     = "raad"
  description = "description"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "efimok@gmail.com"
}

variable "db_username" {
  description = "Username for RDS"
}

variable "db_password" {
  description = "Password for db"
}
